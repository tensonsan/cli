## Command-line interface (CLI) for embedded systems

### Features:

* No dynamic allocations.
* Portable.
* VT100 compatible.
* Autocomplete support (optional).
* Command history support (optional).
* C99 compliant.
* Unit tested.

### Porting:

* Copy [cli.c](cli.c), [cli.h](cli.h) and [cli_config.h](cli_config.h) to your project.
* Edit the [cli_config.h](cli_config.h) according to your needs (don't forget to check the key mappings)
* Provide CLI_PRINTF and CLI_ASSERT macros in [cli_config.h](cli_config.h).

### Usage:

Generate Doxygen documentation for the library.
```Makefile
make doc
```

Compile the library into an executable, used in unit tests.
```Makefile
make
```

Run the unit tests ([Unity framework](http://www.throwtheswitch.org/unity) is used).
```Makefile
make run
```

Generate the unit test coverage report.
```Makefile
make coverage
```

Cleanup
```Makefile
make clean
```

### Howto

To used colored outputs (e.g. in case of CLI errors) in Minicom:
```Bash
minicom -D /dev/ttyUSB0 -c on
```

### License:

Copyright 2019 Gasper Korinsek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


