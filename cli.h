
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Command Line Interface
 *
 **********************************************************************/

/**
 * @addtogroup CLI
 * @{
 */

#ifndef _CLI_H_
#define _CLI_H_

#include <stdint.h>
#include "cli_config.h"

/// Maximal escape sequence length
#define CLI_ESCAPE_SEQUENCE_LEN 5

#ifdef CLI_USE_HELP
#define CLI_HELP(x) ,(x)
#else
#define CLI_HELP(x)
#endif

#define CLI_JOB(NAME)       NAME##_cli_job
#define CLI_CMD(NAME, HELP) {#NAME, NAME##_cli_job CLI_HELP(HELP)}
#define CLI_CMD_END         {NULL, NULL CLI_HELP(NULL)}

/// CLI command job
typedef int32_t (*cli_job_t)(int32_t argc, char *argv[]);

/// CLI escape sequence job
typedef void (*cli_es_job_t)(void *arg);

/// CLI command
typedef struct cli_cmd
{
    const char *cmd;    ///< Command string
    cli_job_t job;      ///< Command job
#ifdef CLI_USE_HELP
    const char *help;   ///< Command description string
#endif
} cli_cmd_t;

/// CLI escape sequence
typedef struct cli_es
{
    const char *sequence;   ///< Escape sequence string
    cli_es_job_t job;       ///< Escape sequence job
} cli_es_t;

/// CLI history handler
typedef struct cli_history
{
    char buffer[CLI_HISTORY_MAX][CLI_BUFFER_LEN];   ///< History buffer
    int32_t count;  ///< Number of commands  in history buffer
    int32_t wr;     ///< History buffer write index
    int32_t rd;     ///< History buffer read index
    int32_t index;  ///< Current history buffer index
} cli_history_t;

/// CLI escape sequence parser
typedef struct cli_escape
{
    char buffer[CLI_ESCAPE_SEQUENCE_LEN];   ///< Escape sequence buffer
    int32_t length;     ///< Length of the escape sequence buffer
    int32_t max_len;    ///< Maximal escape sequence table entry length
    cli_es_t *table;    ///< Table of escape sequences
} cli_escape_t;

/// CLI handle
typedef struct cli_handle
{
    char buffer[CLI_BUFFER_LEN];    ///< Command buffer
    int32_t length;     ///< Length of the command buffer
    int32_t position;   ///< Cursor position
    cli_cmd_t *table;   ///< Table of commands
    int32_t argc;       ///< Number of command arguments
    char *argv[CLI_ARGS_MAX];   ///< Table of pointers to command argument strings
    cli_escape_t escape;        ///< CLI escape sequence
#ifdef CLI_USE_HISTORY
    cli_history_t history;      ///< CLI history
#endif
} cli_handle_t;

void cli_init(cli_handle_t *h, const cli_cmd_t *table);
void cli_process(cli_handle_t *h, char ch);
void cli_print_help(cli_handle_t *h);

#endif

/**
 * @}
 */
