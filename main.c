
#ifdef USE_TESTS

#include <stdio.h>
#include <string.h>
#include "cli.h"
#include "unity.h"
#include <stdarg.h>

#define TEST_BUFFER_SIZE 4096
#define TEST_KEY_UP     "\e[A"
#define TEST_KEY_DOWN   "\e[B"
#define TEST_KEY_RIGHT  "\e[C"
#define TEST_KEY_LEFT   "\e[D"

static int32_t CLI_JOB(foo)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(bar)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(foozi)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(quit)(int32_t argc, char *argv[]);

static char test_buffer[TEST_BUFFER_SIZE];
static char *top = test_buffer;
static cli_handle_t cli_hnd;

static const cli_cmd_t cli_table[] = 
{
    CLI_CMD(foo, "Does foo"),
    CLI_CMD(bar, "Does bar"),
    CLI_CMD(foozi, "Does foozi"),
    CLI_CMD(quit, "Quits"),
    CLI_CMD_END
};

static void cli_test_printf_reset(void)
{
    top = test_buffer;
}

static void cli_print_args(int32_t argc, char *argv[])
{
    CLI_PRINTF("\r\n");
    for (int32_t i=0; i < argc; ++i)
    {
        CLI_PRINTF("[%s]", argv[i]);
    }
}

void cli_test_printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);

    vsprintf(top, format, args);
    top += strlen(top);

    va_end(args);
}

static int32_t CLI_JOB(foo)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv); 

    return 0;
}

static int32_t CLI_JOB(bar)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv);

    return 0;
}

static int32_t CLI_JOB(foozi)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv);

    return 0;
}

static int32_t CLI_JOB(quit)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv);

    return 0;
}

static void cli_run(const char *input, const char *expected)
{
    char *s = (char *)input;
    while (*s)
    {
        cli_process(&cli_hnd, *s);
        s++;
    }

    TEST_ASSERT_EQUAL_STRING(expected, test_buffer);

    cli_test_printf_reset();
}

static void test_cli_basic(void)
{
    cli_init(&cli_hnd, cli_table);

    // empty command
    cli_run("\r", "\r\n>\r\n>");
    cli_run("\r", "\r\n>");

    // unknown command
    cli_run("ta\r", "ta\r\nE: cmd unknown\r\n>");

    // known command, zero arguments
    cli_run("foo\r", "foo\r\n[foo]\r\ncmd: 0\r\n>");

    // known command, several arguments
    cli_run("foo 1 2 test\r", "foo 1 2 test\r\n[foo][1][2][test]\r\ncmd: 0\r\n>");

    // known commandm, too many arguments
    cli_run("foo 1 2 3 4 5 6\r", "foo 1 2 3 4 5 6\r\nE: cmd arguments\r\n>");

    // too long command
    cli_run("this is a too long command so handle it", "this is a too long command so ha\r\nE: cmd length\r\n>ndle it");
}

static void test_cli_autocomplete(void)
{
    // multiple hits, command match
    cli_init(&cli_hnd, cli_table);
    cli_run("foo\t", "\r\n>foo\r\nfoo\r\nfoozi\r\n>foo");

    // multiple hits, no command match
    cli_init(&cli_hnd, cli_table);
    cli_run("fo\t", "\r\n>fo\r\nfoo\r\nfoozi\r\n>fo");

    // single hit, command match and confirm
    cli_init(&cli_hnd, cli_table);
    cli_run("fooz\t\r", "\r\n>foozi\r\n[foozi]\r\ncmd: 0\r\n>");

    // no hits, command match with arguments
    cli_init(&cli_hnd, cli_table);
    cli_run("foo 1\t", "\r\n>foo 1");
}

static void test_cli_delete(void)
{
    cli_init(&cli_hnd, cli_table);

    // single char deletion to get a match
    cli_run("barz\x7f\r", "\r\n>barz\x08 \x08\r\n[bar]\r\ncmd: 0\r\n>");

    // delete more chars than the command length
    cli_run("foo\x7f\x7f\x7f\x7f\r", "foo\x08 \x08\x08 \x08\x08 \x08\r\n>");
}

static void test_cli_escape_sequence(void)
{
    cli_init(&cli_hnd, cli_table);

    // Normal escape sequence
    cli_run("\e[A", "\r\n>");

    // Glued escape sequence and command
    cli_run("\e[Afoo\r", "foo\r\n[foo]\r\ncmd: 0\r\n>");

    // Glued command and escape sequence
    cli_run("foo\e[A", "foo\r\n>foo");

    // Too long escape sequence
    cli_run("\elongescape", "ngescape");
}

static void test_cli_history(void)
{
    cli_init(&cli_hnd, cli_table);

    // No history
    cli_run(TEST_KEY_UP, "\r\n>");
    cli_run(TEST_KEY_DOWN, "\r\n>");

    // Zero command
    cli_run("\r", "\r\n>");
    cli_run(TEST_KEY_UP, "\r\n>");
    cli_run(TEST_KEY_DOWN, "\r\n>");

    // Single item
    cli_run("foo\r", "foo\r\n[foo]\r\ncmd: 0\r\n>");
    cli_run(TEST_KEY_UP, "\r\n>foo");
    cli_run(TEST_KEY_UP, "\r\n>foo");
    cli_run(TEST_KEY_DOWN, "\r\n>");
    cli_run(TEST_KEY_DOWN, "\r\n>");

    // Two items
    cli_run("bar\r", "bar\r\n[bar]\r\ncmd: 0\r\n>");
    cli_run(TEST_KEY_UP, "\r\n>bar");
    cli_run(TEST_KEY_UP, "\r\n>foo");
    cli_run(TEST_KEY_UP, "\r\n>foo");
    cli_run(TEST_KEY_DOWN, "\r\n>bar");
    cli_run(TEST_KEY_DOWN, "\r\n>");

    // Four items (oldest history removed)
    cli_run("quit\r", "quit\r\n[quit]\r\ncmd: 0\r\n>");
    cli_run("foozi\r", "foozi\r\n[foozi]\r\ncmd: 0\r\n>");
    cli_run(TEST_KEY_UP, "\r\n>foozi");
    cli_run(TEST_KEY_UP, "\r\n>quit");
    cli_run(TEST_KEY_UP, "\r\n>bar");
    cli_run(TEST_KEY_UP, "\r\n>bar");
    cli_run(TEST_KEY_DOWN, "\r\n>quit");
    cli_run(TEST_KEY_DOWN, "\r\n>foozi");
    cli_run(TEST_KEY_DOWN, "\r\n>");
}

static void test_cli_cursor_insert(void)
{
    char in[256], out[256];

    // Insert beginning
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s%sb", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s%s\r>bfoo\b\b\b", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);

    // Insert beginning, test limit
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s%s%sb", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s%s\r>bfoo\b\b\b", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);

    // Insert middle
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%sb", TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s\r>fboo\b\b", TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);

    // Insert end, test limit
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%sb", TEST_KEY_RIGHT);
    sprintf(out, "\r\n>foob");
    cli_run(in, out);

    // Insert after cursor movement
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s%sb", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_RIGHT);
    sprintf(out, "\r\n>foo%s%s%s\r>fobo\b", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_RIGHT);
    cli_run(in, out);

    // Insert many
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%sbar", TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s\r>fobo\b\r>fobao\b\r>fobaro\b", TEST_KEY_LEFT);
    cli_run(in, out);
}

static void test_cli_cursor_delete(void)
{
    char in[256], out[256];

    // Delete beginning
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s\x7f", TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s\r>oo \b\b\b", TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);

    // Delete beginning, test limit
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s%s\x7f", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s%s\r>oo \b\b\b", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);

    // Delete middle
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s\x7f", TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s\r>fo \b\b", TEST_KEY_LEFT);
    cli_run(in, out);

    // Delete all, start from end
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo\x7f\x7f\x7f");
    sprintf(out, "\r\n>foo\b \b\b \b\b \b");
    cli_run(in, out);

    // Delete all, start from beginning
    cli_init(&cli_hnd, cli_table);

    sprintf(in, "foo%s%s%s\x7f\x7f\x7f", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    sprintf(out, "\r\n>foo%s%s%s\r>oo \b\b\b\r>o \b\b\r> \b", TEST_KEY_LEFT, TEST_KEY_LEFT, TEST_KEY_LEFT);
    cli_run(in, out);
}

static void test_cli_help(void)
{
    cli_init(&cli_hnd, cli_table);

    cli_print_help(&cli_hnd);

    TEST_ASSERT_EQUAL_STRING("\r\n>\r\nfoo Does foo\r\nbar Does bar\r\nfoozi Does foozi\r\nquit Quits", test_buffer);
}

void setUp(void)
{
    memset(test_buffer, 0, sizeof(test_buffer));
}

void tearDown(void)
{
}

#else

#include <stdio.h>
#include <string.h>
#include "cli.h"
#include <stdarg.h>
#include <termios.h>

static int32_t CLI_JOB(led)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(test)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(exit)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(help)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(leverage)(int32_t argc, char *argv[]);

static int32_t cli_done = 0;
static struct termios old, current;
static cli_handle_t cli_hnd;

static const cli_cmd_t cli_table[] = 
{
    CLI_CMD(led, "Controls LED"),
    CLI_CMD(test, "Tests stuff"),
    CLI_CMD(exit, "Exits"),
    CLI_CMD(help, "Displays help"),
    CLI_CMD(leverage, "Leverages stuff"),
    CLI_CMD_END
};

static void cli_print_args(int32_t argc, char *argv[])
{
    CLI_PRINTF("\r\n");
    for (int32_t i=0; i < argc; ++i)
    {
        CLI_PRINTF("[%s]", argv[i]);
    }
}

static void termios_init(void) 
{
    tcgetattr(0, &old);

    current = old;

    cfmakeraw(&current);

    tcsetattr(0, TCSANOW, &current);
}

static void termios_reset(void) 
{
    tcsetattr(0, TCSANOW, &old);
}

static char getch(void) 
{
    char ch;

    termios_init();

    ch = getchar();

    termios_reset();

    return ch;
}

static int32_t CLI_JOB(help)(int32_t argc, char *argv[])
{
    cli_print_help(&cli_hnd);

    return 0;
}

static int32_t CLI_JOB(led)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv); 

    return 0;
}

static int32_t CLI_JOB(test)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv); 

    return 0;
}

static int32_t CLI_JOB(leverage)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv); 

    return 0;
}

static int32_t CLI_JOB(exit)(int32_t argc, char *argv[])
{
    cli_print_args(argc, argv); 

    cli_done = 1;

    return 0;
}

#endif

int main(int argc, char *argv[])
{
#ifdef USE_TESTS
    UNITY_BEGIN();

    RUN_TEST(test_cli_basic);
    RUN_TEST(test_cli_autocomplete);
    RUN_TEST(test_cli_delete);
    RUN_TEST(test_cli_escape_sequence);
    RUN_TEST(test_cli_history);
    RUN_TEST(test_cli_cursor_insert);
    RUN_TEST(test_cli_cursor_delete);
    RUN_TEST(test_cli_help);

    return UNITY_END();
#else
    cli_init(&cli_hnd, cli_table);

    while (!cli_done)
    {
        char ch = getch();
        cli_process(&cli_hnd, ch);
    }

    return 0;
#endif
}
